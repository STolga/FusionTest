using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Fusion;
using TMPro;
using UnityEngine.UI;
using System;

public enum PanelTypes { LobbyEntrance, Lobby, CreateSession, CurrentSession}
public class MainMenuUIManager : MonoBehaviour
{
    //for placing player avatars in current session
    [SerializeField] private GameObject singlePlayerAvatarPrefab;
    [SerializeField] private Transform playerAvatarsContentParent;
    [SerializeField] private GameObject plyerReadyCheckmark;
    [SerializeField] private Slider redSlider;
    [SerializeField] private Slider greenSlider;
    [SerializeField] private Slider blueSlider;
    [SerializeField] private Image colorArea;
    private List<SinglePlayerAvatarItem> playerAvatars;
    private bool updatePlayers = false;

    //Start Button
    [SerializeField] private Button startButton;
    [SerializeField] private TMP_Text startButtonLabel;

    //for placing sessions in current lobby
    [SerializeField] private GameObject singleSessionPrefab;
    [SerializeField] private Transform sessionsContentParent;

    //MAx player count in "Create New Session" panel
    [SerializeField] private TMP_InputField playerCountInput;

    //Panels
    [SerializeField] private GameObject enterLobbyPanel;
    [SerializeField] private GameObject lobbyPanel;
    [SerializeField] private GameObject allSessionsPanel;
    [SerializeField] private GameObject createSessionPanel;
    [SerializeField] private GameObject currentSessionPanel;

    private void OnEnable()
    {
        MainApp.onSessionListUpdated += SessionListUpdated;
        MainApp.onNewPlayerJoined += PlayerJoined;
        MainApp.onPlayerExited += PlayerExited;
    }

    void Start()
    {
        playerAvatars = new List<SinglePlayerAvatarItem>();
        plyerReadyCheckmark.SetActive(false);
        ActivatePanel(PanelTypes.LobbyEntrance);
    }

    private void Update()
    {
        if(MainApp.Instance.ConnectionStatus != ConnectionStatus.Disconnected &&
            MainApp.Instance.networkRunner.Simulation != null)
        {
            UpdatePlayersInSession();
            var ready = 0;
            foreach (Player ply in MainApp.Instance.Players)
            {
                if (ply.Ready)
                    ready++;
            }

            string wait = null;
            if (ready < MainApp.Instance.networkRunner.Simulation.Config.DefaultPlayers)
                wait = $"Waiting for {MainApp.Instance.networkRunner.Simulation.Config.DefaultPlayers - ready} of {MainApp.Instance.networkRunner.Simulation.Config.DefaultPlayers} players";
            else if (!MainApp.Instance.IsMaster)
                wait = "Waiting for master to start";

            startButton.enabled = wait == null;
            startButtonLabel.text = wait ?? "Start";
        }
    }

    private void UpdatePlayersInSession()
    {
        if(updatePlayers)
        {
            UpdatePlayerAvatarList();
        }
    }

    private void PlayerExited(int playerID)
    {
        Debug.Log("***** Player left: " + playerID);
    }

    private void PlayerJoined()
    {
        Debug.Log("***** New Player Joined *****");
    }

    private void UpdatePlayerAvatarList()
    {
        playerAvatars.Clear();
        DeleteChilds(playerAvatarsContentParent);
        foreach (Player player in MainApp.Instance.Players)
        {
            var playerAvatar = Instantiate(singlePlayerAvatarPrefab, playerAvatarsContentParent).GetComponent<SinglePlayerAvatarItem>();
            playerAvatars.Add(playerAvatar);
            playerAvatar.Setup(player);
        }
    }

    public void OnToggleIsReady()
    {
        Player ply = MainApp.Instance.GetPlayer();
        plyerReadyCheckmark.SetActive(!ply.Ready);
        ply.RPC_SetIsReady(!ply.Ready);
    }

    public void OnNameChanged(string name)
    {
        Player ply = MainApp.Instance.GetPlayer();
        ply.RPC_SetName(name);
    }

    public void OnColorUpdated()
    {
        Player ply = MainApp.Instance.GetPlayer();
        Color c = new Color(redSlider.value, greenSlider.value, blueSlider.value);
        colorArea.color = c;
        ply.RPC_SetColor(c);
    }

    private void SessionListUpdated(List<SessionInfo> obj)
    {
        DeleteChilds(sessionsContentParent);
        Debug.Log("******Session List Is Updated*****");
        foreach (SessionInfo sI in obj)
        {
            Debug.Log(sI.PlayerCount + " " + sI.MaxPlayers);
            var sessionPrefab = Instantiate(singleSessionPrefab, sessionsContentParent).GetComponent<SingleSessionItem>();
            sessionPrefab.Setup(sI);
        }
    }

    public void JoinLobby()
    {
        MainApp.Instance.EnterLobby("Standart");
        ActivatePanel(PanelTypes.Lobby);
    }
    public void CreateSessionButtonPressed()
    {
        ActivatePanel(PanelTypes.CreateSession);
    }
    public void CreateSession()
    {
        ActivatePanel(PanelTypes.CurrentSession);
        MainApp.Instance.CreateSession(int.Parse(playerCountInput.text));
    }

    public void CancelCreateSessionButton()
    {
        ActivatePanel(PanelTypes.Lobby);
    }

    public void JoinSession(string roomName)
    {
        ActivatePanel(PanelTypes.CurrentSession);
        MainApp.Instance.JoinSession(roomName);
    }

    public void StartGameButtonPressed()
    {
        MainApp.Instance.Session.LoadGameScene();
    }

    public void Disconnect()
    {
        MainApp.Instance.Disconnect();
        ActivatePanel(PanelTypes.LobbyEntrance);
    }

    private void ActivatePanel(PanelTypes currPanel)
    {
        updatePlayers = false;
        CloseAllPanels();
        switch (currPanel)
        {
            case PanelTypes.CreateSession:
                lobbyPanel.SetActive(true);
                createSessionPanel.SetActive(true);
                break;
            case PanelTypes.Lobby:
                lobbyPanel.SetActive(true);
                allSessionsPanel.SetActive(true);
                break;
            case PanelTypes.CurrentSession:
                updatePlayers = true;
                lobbyPanel.SetActive(true);
                currentSessionPanel.SetActive(true);
                break;
            case PanelTypes.LobbyEntrance:
                enterLobbyPanel.SetActive(true);
                break;
        }
    }

    private void CloseAllPanels()
    {
        enterLobbyPanel.SetActive(false);
        lobbyPanel.SetActive(false);
        allSessionsPanel.SetActive(false);
        createSessionPanel.SetActive(false);
        currentSessionPanel.SetActive(false);
    }


    private void DeleteChilds(Transform currParent)
    {
        foreach(Transform currChild in currParent)
        {
            Destroy(currChild.gameObject);
        }
    }
    private void OnDisable()
    {
        MainApp.onSessionListUpdated -= SessionListUpdated;
        MainApp.onNewPlayerJoined -= PlayerJoined;
        MainApp.onPlayerExited -= PlayerExited;
    }

}
