using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Fusion;

public class SingleSessionItem : MonoBehaviour
{
	[SerializeField] private SessionInfo relatedSessionInfo;
	[SerializeField] private Button joinButton;
	[SerializeField] private TMP_Text sessionName;
	[SerializeField] private TMP_Text sessionPlayers;
	private MainMenuUIManager mainMenuUIManager;
	public void Setup(SessionInfo sessionInfo)
	{
		mainMenuUIManager = FindObjectOfType<MainMenuUIManager>();
		relatedSessionInfo = sessionInfo;
		sessionName.text = relatedSessionInfo.Name;
		sessionPlayers.text = relatedSessionInfo.PlayerCount + "/" + relatedSessionInfo.MaxPlayers;
	}

	public void JoinSession()
    {
		if(mainMenuUIManager)
        {
			mainMenuUIManager.JoinSession(relatedSessionInfo.Name);
        }
    }
}
