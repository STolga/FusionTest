using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Fusion;
using Fusion.Sockets;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum ConnectionStatus
{
	Disconnected,
	Connecting,
	Connected,
	Failed,
	EnteringLobby,
	InLobby,
	Starting,
	Started
}

/// <summary>
/// This is the main entry point for the application. App is a singleton created when the game is launched.
/// Access it anywhere using `App.Instance`
/// </summary>

[RequireComponent(typeof(NetworkSceneManagerBase))]
public class MainApp : MonoBehaviour, INetworkRunnerCallbacks
{
	public static Action<List<SessionInfo>> onSessionListUpdated;
	public static Action onNewPlayerJoined;
	public static Action<int> onPlayerExited;

	public NetworkRunner networkRunner;
	[SerializeField] private Player _playerPrefab;
	[SerializeField] private Session sessionPrefab;
	[SerializeField] private SceneReference _introScene;

	private string _lobbyId;
	private readonly Dictionary<PlayerRef, Player> _players = new Dictionary<PlayerRef, Player>();
	public ICollection<Player> Players => _players.Values;
	private Session session;
	private NetworkInputData inputData;

	private static MainApp _instance;

	public static MainApp Instance
	{
		get
		{
			if (_instance == null)
				_instance = FindObjectOfType<MainApp>();
			return _instance;
		}
	}

	public ConnectionStatus ConnectionStatus { get; private set; }
	public bool IsMaster => networkRunner != null && (networkRunner.IsServer || networkRunner.IsSharedModeMasterClient);

	private void Awake()
	{
		if (_instance == null)
			_instance = this;

		if (_instance != this)
		{
			Destroy(gameObject);
		}
		else
		{
			DontDestroyOnLoad(gameObject);
			SceneManager.LoadSceneAsync(_introScene);
		}
	}

    private void Connect()
	{
		if (networkRunner == null)
		{
			SetConnectionStatus(ConnectionStatus.Connecting);
			GameObject go = new GameObject("Session");
			go.transform.SetParent(transform);

			networkRunner = go.AddComponent<NetworkRunner>();
			networkRunner.AddCallbacks(this);
		}
	}

	public void Disconnect()
	{
		if (networkRunner != null)
		{
			SetConnectionStatus(ConnectionStatus.Disconnected);
			networkRunner.Shutdown();
		}
	}
	public void CreateSession(int playerCount)
	{
		HostSession(playerCount);
	}
	public void JoinSession(string roomName)
	{
		ClientSession(roomName);
	}

	private void HostSession(int playerLimit)
	{
		Connect();

		SetConnectionStatus(ConnectionStatus.Starting);

		Debug.Log($"Starting game with random session, player limit {playerLimit}");

		//networkRunner.ProvideInput = mode != GameMode.Server;
		networkRunner.ProvideInput = true;
		networkRunner.StartGame(new StartGameArgs
		{
			GameMode = GameMode.Host,
			SceneManager = gameObject.AddComponent<NetworkSceneManagerDefault>(),
			PlayerCount = playerLimit,
			DisableClientSessionCreation = false
			//CustomLobbyName = "CustomLobby",
			//SessionName = roomName,
		});
	}
	private void ClientSession(string roomName)
	{
		Connect();

		SetConnectionStatus(ConnectionStatus.Starting);

		Debug.Log($"Joining game with session {roomName}, player limit ");

		//networkRunner.ProvideInput = mode != GameMode.Server;
		networkRunner.ProvideInput = true;

		networkRunner.StartGame(new StartGameArgs
		{
			GameMode = GameMode.Client,
			SceneManager = gameObject.AddComponent<NetworkSceneManagerDefault>(),
			SessionName = roomName,
			DisableClientSessionCreation = false
			//CustomLobbyName = "CustomLobby",
		});
	}
	public async Task EnterLobby(string lobbyID)
	{
		Connect();

		_lobbyId = lobbyID;

		SetConnectionStatus(ConnectionStatus.EnteringLobby);
		var result = await networkRunner.JoinSessionLobby(SessionLobby.Custom, _lobbyId);

		if (!result.Ok)
		{
			onSessionListUpdated = null;
			SetConnectionStatus(ConnectionStatus.Failed);
		}
	}
	private void SetConnectionStatus(ConnectionStatus status, string reason = "")
	{
		if (ConnectionStatus == status)
			return;
		ConnectionStatus = status;
		Debug.Log($"ConnectionStatus={status} {reason}");
	}

	public void SetPlayer(PlayerRef playerRef, Player player)
	{
		_players[playerRef] = player;
		player.transform.SetParent(session.transform);
	}

	public Player GetPlayer(PlayerRef ply = default)
	{
		if (!networkRunner)
			return null;
		if (ply == default)
			ply = networkRunner.LocalPlayer;
		_players.TryGetValue(ply, out Player player);
		return player;
	}
	public Session Session
	{
		get => session;
		set { session = value; session.transform.SetParent(networkRunner.transform); }
	}

	/// <summary>
	/// Fusion Event Handlers
	/// </summary>

	public void OnConnectedToServer(NetworkRunner runner)
	{
		Debug.Log("Connected to server");
		SetConnectionStatus(ConnectionStatus.Connected);
	}
	public void OnDisconnectedFromServer(NetworkRunner runner)
	{
		Debug.Log("Disconnected from server");
		Disconnect();
	}
	public void OnConnectFailed(NetworkRunner runner, NetAddress remoteAddress, NetConnectFailedReason reason)
	{
		Debug.Log($"Connect failed {reason}");
		Disconnect();
		SetConnectionStatus(ConnectionStatus.Failed, reason.ToString());
	}
	public void OnPlayerJoined(NetworkRunner runner, PlayerRef playerRef)
	{
		Debug.Log($"Player {playerRef} Joined!");

		if (session == null && IsMaster)
		{
			Debug.Log("Spawning world");
			session = runner.Spawn(sessionPrefab, Vector3.zero, Quaternion.identity);
		}

		if (runner.IsServer || runner.Topology == SimulationConfig.Topologies.Shared && playerRef == runner.LocalPlayer)
		{
			Debug.Log("Spawning player");
			runner.Spawn(_playerPrefab, Vector3.zero, Quaternion.identity, playerRef);
		}
		onNewPlayerJoined?.Invoke();
        SetConnectionStatus(ConnectionStatus.Started);
	}
	public void OnPlayerLeft(NetworkRunner runner, PlayerRef player)
	{
		Debug.Log($"{player.PlayerId} disconnected.");
		onPlayerExited?.Invoke(player.PlayerId);
	}
	public void OnShutdown(NetworkRunner runner, ShutdownReason reason)
	{
		Debug.Log($"OnShutdown {reason}");
		SetConnectionStatus(ConnectionStatus.Disconnected, reason.ToString());

		if (networkRunner != null && networkRunner.gameObject)
			Destroy(networkRunner.gameObject);

		networkRunner = null;

		if (Application.isPlaying)
			SceneManager.LoadSceneAsync(_introScene);
	}
	public void OnConnectRequest(NetworkRunner runner, NetworkRunnerCallbackArgs.ConnectRequest request, byte[] token)
	{
		request.Accept();
	}
	public void OnSessionListUpdated(NetworkRunner runner, List<SessionInfo> sessionList)
	{
		Debug.Log("**************Session List Updated. Recieving updated list*********");
		SetConnectionStatus(ConnectionStatus.InLobby);
		onSessionListUpdated?.Invoke(sessionList);
	}
	public void OnInput(NetworkRunner runner, NetworkInput input)
	{
		var movementController = FindObjectOfType<PlayerMovementController>();
		NetworkInputData inputData = new NetworkInputData();
        if (movementController)
        {
            float verticalAxis;
            float horizontalAxis;
            verticalAxis = Input.GetAxis("Vertical");
            horizontalAxis = Input.GetAxis("Horizontal");
            inputData.vertical = verticalAxis;
            inputData.horizontal = horizontalAxis;

            if (Input.GetKeyDown(KeyCode.F))
            {
                inputData.isFired = true;
            }
        }

        input.Set(inputData);
	}
	public void OnInputMissing(NetworkRunner runner, PlayerRef player, NetworkInput input) { }
	public void OnUserSimulationMessage(NetworkRunner runner, SimulationMessagePtr message) { }
	public void OnCustomAuthenticationResponse(NetworkRunner runner, Dictionary<string, object> data) { }
	public void OnHostMigration(NetworkRunner runner, HostMigrationToken hostMigrationToken) { }
	public void OnReliableDataReceived(NetworkRunner runner, PlayerRef player, ArraySegment<byte> data) { }
	public void OnSceneLoadDone(NetworkRunner runner) { }
	public void OnSceneLoadStart(NetworkRunner runner) { }
}