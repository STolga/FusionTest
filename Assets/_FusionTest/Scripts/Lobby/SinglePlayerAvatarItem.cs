using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SinglePlayerAvatarItem : MonoBehaviour
{
	[SerializeField] private TMP_Text name;
	[SerializeField] private Image color;
	[SerializeField] private GameObject ready;

	public void Setup(Player ply)
	{
		name.text = ply.Name.Value;
		color.color = ply.Color;
		ready.SetActive(ply.Ready);
	}
}
