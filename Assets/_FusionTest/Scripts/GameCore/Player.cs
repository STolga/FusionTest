using Fusion;
using UnityEngine;
public class Player : NetworkBehaviour
{
    private NetworkCharacterControllerPrototype _cc;
	public PlayerController playerController;

	[SerializeField] public PlayerController characterPrefab;
	[Networked] public NetworkString<_32> Name { get; set; }
	[Networked] public Color Color { get; set; }
	[Networked] public NetworkBool Ready { get; set; }
	[Networked] public NetworkBool InputEnabled { get; set; }

	private void Awake()
    {
        _cc = GetComponent<NetworkCharacterControllerPrototype>();
	}

	public override void Spawned()
	{
		MainApp.Instance.SetPlayer(Object.InputAuthority, this);
	}

	[Rpc(RpcSources.InputAuthority, RpcTargets.StateAuthority)]
	public void RPC_SetIsReady(NetworkBool ready)
	{
		Ready = ready;
	}

	[Rpc(RpcSources.InputAuthority, RpcTargets.StateAuthority)]
	public void RPC_SetName(NetworkString<_32> name)
	{
		Name = name;
	}

	[Rpc(RpcSources.InputAuthority, RpcTargets.StateAuthority)]
	public void RPC_SetColor(Color color)
	{
		Color = color;
	}

	public override void FixedUpdateNetwork()
    {
        //if (GetInput(out NetworkInputData data))
        //{
        //    data.direction.Normalize();
        //    _cc.Move(5 * data.direction * Runner.DeltaTime);
        //}
    }
}