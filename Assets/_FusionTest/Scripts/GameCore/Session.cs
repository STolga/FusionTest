using System.Collections.Generic;
using Fusion;

/// <summary>
/// Session gets created when a game session starts and exists in only one instance.
/// It survives scene loading and can be used to control game-logic inside a session, across scenes.
/// </summary>

public class Session : NetworkBehaviour
{
	[Networked] public TickTimer PostLoadCountDown { get; set; }
	public SessionInfo Info => Runner.SessionInfo;
	public LevelNetworkPlayersManager levelNetworkPlayersManager;
	private HashSet<PlayerRef> _finishedLoading = new HashSet<PlayerRef>();

	public override void Spawned()
	{
		MainApp.Instance.Session = this;
	}

	[Rpc(RpcSources.All, RpcTargets.StateAuthority, Channel = RpcChannel.Reliable)]
	public void RPC_FinishedLoading(PlayerRef playerRef)
	{
		_finishedLoading.Add(playerRef);
		if (_finishedLoading.Count >= MainApp.Instance.Players.Count)
		{
			PostLoadCountDown = TickTimer.CreateFromSeconds(Runner, 3);
		}
	}

	public override void FixedUpdateNetwork()
	{
		if (PostLoadCountDown.Expired(Runner))
		{
			PostLoadCountDown = TickTimer.None;
			foreach (Player player in MainApp.Instance.Players)
				player.InputEnabled = true;
		}
	}

	public void LoadGameScene()
	{
		foreach (Player player in MainApp.Instance.Players)
			player.InputEnabled = false;
		Runner.SetActiveScene(1);
	}

}