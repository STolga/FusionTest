using System.Collections;
using System.Collections.Generic;
using Fusion;
using UnityEngine;
using DG.Tweening;
using System;

public class BulletBehaviour : NetworkBehaviour
{
	public override void Spawned()
	{

	}

	public void Init()
    {
		transform.DOMove(transform.position + transform.up * 20f, 3f).SetSpeedBased().OnComplete(BulletComplete);
    }

    private void BulletComplete()
    {
		Runner.Despawn(Object);
	}
}
