using Fusion;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
/// <summary>
/// Visual representation of a Player - the Character is instantiated by the map once it's loaded.
/// This class handles camera tracking and player movement and is destroyed when the map is unloaded.
/// (I.e. the player gets a new avatar in each map)
/// </summary>

public class PlayerController : NetworkBehaviour
{
	[SerializeField] private SpriteRenderer playerImage;
	private Transform _camera;
	private Player _player;
	private PlayerMovementController _spaceshipController = null;
	[SerializeField] private TMP_Text playerNameText;
	[SerializeField] private TMP_Text playerLifeText;

	//Shoot variables
	[SerializeField] private GameObject bulletPrefab;

	[Networked(OnChanged = nameof(OnLivesChanged))] public int life { get; private set; }

	public static void OnLivesChanged(Changed<PlayerController> playerInfo)
	{
		playerInfo.Behaviour.UpdateLife();
	}

	public override void Spawned()
	{
		_player = MainApp.Instance.GetPlayer(Object.InputAuthority);
		//_name.text = _player.Name.Value;
		if(playerImage)
			playerImage.color = _player.Color;
		playerNameText.text = _player.Name.Value;
		InitPLayer();
		UpdateLife();
		_spaceshipController = GetComponent<PlayerMovementController>();


		// --- Host
		// The Game Session SPECIFIC settings are initialized
		if (Object.HasStateAuthority == false) return;
	}
	private void InitPLayer()
	{
		life = 100;
	}
	//[Rpc(RpcSources.All, RpcTargets.All)]
	public void RPC_TakeDamage(int damage)
	{
		life -= damage;
	}
	public void UpdateLife()
    {
		playerLifeText.text = life.ToString();
    }

    //public void LateUpdate()
    //{
    //	if (Object.HasInputAuthority)
    //	{
    //		if (_camera == null)
    //			_camera = Camera.main.transform;
    //		Transform t = transform;
    //		Vector3 p = t.position;
    //		_camera.position = p - 10 * t.forward + 5 * Vector3.up;
    //		_camera.LookAt(p + 2 * Vector3.up);
    //	}
    //}

    public override void FixedUpdateNetwork()
	{
		// GetInput() can only be called from NetworkBehaviours.
		// In SimulationBehaviours, either TryGetInputForPlayer<T>() or GetInputForPlayer<T>() has to be called.
		// This will only return true on the Client with InputAuthority for this Object and the Host.
		//if (Runner.TryGetInputForPlayer<NetworkInputData>(Object.InputAuthority, out var input))
		//{
		//	Move(input);
		//}

		if(Runner.TryGetInputForPlayer(Object.InputAuthority, out NetworkInputData networkInputData))
        {
			if(networkInputData.isFired)
            {
				Shoot();
			}
        }
	}

	private void Shoot()
	{
		if (Object.HasStateAuthority == false) return;

		var currentBullet = Runner.Spawn(bulletPrefab, transform.position - new Vector3(0f,0f,0.5f), Quaternion.identity).GetComponent<BulletBehaviour>();
		currentBullet.Init();
		//var currentBullet = Runner.Spawn(bulletPrefab, transform.position, Quaternion.identity, Object.InputAuthority);
		var enemyNearby = FindClosestPlayer();
		if(enemyNearby)
        {
			//Debug.Log("Shooting At Enemy: " + enemyNearby._player.Name);
			enemyNearby.RPC_TakeDamage(5);
		}
		else
        {
			Debug.Log("There is no enemy NearBy");
        }
	}

	private PlayerController FindClosestPlayer()
    {
		float closestPlayerDist = 10000f;
		PlayerController closestPlayer = null;
		var players = FindObjectsOfType<PlayerController>();
		foreach(PlayerController player in players)
        {
			//Debug.Log("Looking for player: " + player._player.Name);
			if(player.gameObject != gameObject)
            {
				if (Vector2.Distance(transform.position, player.transform.position) < closestPlayerDist)
				{
					closestPlayerDist = Vector2.Distance(transform.position, player.transform.position);
					closestPlayer = player;
				}
			}
        }
		return closestPlayer;
    }


}