using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;
using Fusion;
using System.Xml;
using TMPro;


public class PlayerMovementController : NetworkBehaviour
{
    [SerializeField] private TMP_Text moveDirectionText;
    [SerializeField] private SkeletonAnimation playerSkeletonData;
    [SerializeField] private Spine.AnimationState spineAnimState;
    [SerializeField] private Spine.TrackEntry spineAnimTrackEntry;

    [SerializeField] private Rigidbody2D playerRB;
    [SerializeField] private float playerSpeed = 1f;

    ////Bottom Animation Names
    private string bottomRunAnimName = "run";
    private string bottomIdleAnimName = "idle";

    //Top Animation Names
    private string topRunAnimName = "run-arm";
    private string topIdleAnimName = "idle-arm";
    private string topAttackAnimName = "minning";

    [Networked(OnChanged = nameof(OnMovementSideChanged))] public int moveDir { get; private set; }
    private int oldMoveDir;


    //private int moveDir;
    //private int oldMoveDir;


    // Start is called before the first frame update
    void Start()
    {
        //UpdateMoveDirText();
        InitSpineCharacter();
    }

    public static void OnMovementSideChanged(Changed<PlayerMovementController> playerInfo)
    {
        playerInfo.Behaviour.CheckMoveDirectionChange();
        //playerInfo.Behaviour.UpdateMoveDirText();
    }

    private void UpdateMoveDirText()
    {
        moveDirectionText.text = oldMoveDir.ToString() + "-"+ moveDir.ToString();
    }
    private void InitSpineCharacter()
    {
        if (playerSkeletonData)
        {
            spineAnimState = playerSkeletonData.state;
        }
    }


    public override void FixedUpdateNetwork()
    {
        // GetInput() can only be called from NetworkBehaviours.
        // In SimulationBehaviours, either TryGetInputForPlayer<T>() or GetInputForPlayer<T>() has to be called.
        // This will only return true on the Client with InputAuthority for this Object and the Host.
        //if (Runner.TryGetInputForPlayer<NetworkInputData>(Object.InputAuthority, out var input))
        //{
        //	Move(input);
        //}

        if (Runner.TryGetInputForPlayer(Object.InputAuthority, out NetworkInputData networkInputData))
        {
            Move(networkInputData);
            SetCharacterAnim(networkInputData);
        }
    }


    // Moves the spaceship RB using the input for the client with InputAuthority over the object
    private void Move(NetworkInputData input)
    {
        transform.position += new Vector3(input.horizontal,input.vertical) * Runner.DeltaTime;
    }


    private void SetCharacterAnim(NetworkInputData input)
    {
        if (!playerSkeletonData)
            return;

        if(input.vertical == 0f && input.horizontal == 0f)
        {
            moveDir = -1;
        }
        else
        {
            if (input.vertical > 0f) // goin top
            {
                if (input.horizontal >= 0f) //right
                {
                    moveDir = 0;
                }
                else if (input.horizontal < 0f) //left
                {
                    moveDir = 1;
                }
            }
            else //goin bottom
            {
                if (input.horizontal >= 0f) //right
                {
                    moveDir = 2;
                }
                else if (input.horizontal < 0f) //left
                {
                    moveDir = 3;
                }
            }
        }

        //CheckMoveDirectionChange();
    }

    public void CheckMoveDirectionChange()
    {
        //SwitchAnimState();
        if (oldMoveDir != moveDir)
        {
            SwitchAnimState();
            UpdateMoveDirText();
            oldMoveDir = moveDir;
        }
    }
    private void SwitchAnimState()
    {
        switch (moveDir)
        {
            case -1:
                PlayDefaultIdleAnim();
                break;
            case 0:
                playerSkeletonData.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
                PlayDefaultRunAnim();
                break;
            case 1:
                playerSkeletonData.transform.localEulerAngles = new Vector3(0f, 180f, 0f);
                PlayDefaultRunAnim();
                break;
            case 2:
                playerSkeletonData.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
                PlayDefaultRunAnim();
                break;
            case 3:
                playerSkeletonData.transform.localEulerAngles = new Vector3(0f, 180f, 0f);
                PlayDefaultRunAnim();
                break;
        }
    }
    
    private void PlayDefaultIdleAnim()
    {
        spineAnimTrackEntry = playerSkeletonData.state.SetAnimation(0, bottomIdleAnimName, true);
        spineAnimTrackEntry = playerSkeletonData.state.SetAnimation(1, topIdleAnimName, true);
    }
    private void PlayDefaultRunAnim()
    {
        spineAnimTrackEntry = spineAnimState.SetAnimation(0, bottomRunAnimName, true);
        spineAnimTrackEntry = playerSkeletonData.state.SetAnimation(1, topRunAnimName, true);
    }
}
