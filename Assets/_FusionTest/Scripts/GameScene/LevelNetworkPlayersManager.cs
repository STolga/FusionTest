using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
using Fusion.Sockets;

public class LevelNetworkPlayersManager : SimulationBehaviour, ISpawned
{
	private Dictionary<Player, PlayerController> _playerCharacters = new Dictionary<Player, PlayerController>();

	public void Spawned()
	{
		Debug.Log("Level Network Players Manager spawned");
		// Spawn player avatars
		foreach (Player player in MainApp.Instance.Players)
		{
			SpawnAvatar(player, false);
		}
		// Tell the master that we're done loading
		MainApp.Instance.Session.RPC_FinishedLoading(Runner.LocalPlayer);

		MainApp.Instance.Session.levelNetworkPlayersManager = this;
	}

	public void SpawnAvatar(Player player, bool lateJoiner)
	{
		if (_playerCharacters.ContainsKey(player))
			return;
		if (player.Object.HasStateAuthority) // We have StateAuth over the player if we are the host or if we're the player self in shared mode
		{
			Debug.Log($"Spawning avatar for player {player.Name} with input auth {player.Object.InputAuthority}");
			// Note: This only works if the number of spawnpoints in the map matches the maximum number of players - otherwise there's a risk of spawning multiple players in the same location.
			// For example, with 4 spawnpoints and a 5 player limit, the first player will get index 4 (max-1) and the second will get index 0, and both will then use the first spawn point.

			PlayerController character = Runner.Spawn(player.characterPrefab,
				new Vector3(Random.Range(-5,5f), Random.Range(-2f, 2f),-0.5f), Quaternion.identity, player.Object.InputAuthority);
			_playerCharacters[player] = character;
			//player.playerController = character;
			//player.InputEnabled = lateJoiner;
		}
	}

	public void DespawnAvatar(Player ply)
	{
		if (_playerCharacters.TryGetValue(ply, out PlayerController c))
		{
			Runner.Despawn(c.Object);
			_playerCharacters.Remove(ply);
		}
	}
}
